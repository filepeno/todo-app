from django.db import models
from django.contrib.auth.models import User

class Todo(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.CharField(max_length=200)
    done = models.BooleanField(default=False)
   # priority = models.IntegerField(unique=True)

    #class Meta:
     #   order_by = 'priority'

      #  def highest_priority():
       #     Todo.objects.last()

    def __str__(self):
        return f'{self.pk}: user: {self.user}, text: "{self.text}" done: {self.done}'
# Create your models here.
