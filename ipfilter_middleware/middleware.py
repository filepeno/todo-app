from django.core.exceptions import PermissionDenied
from django.conf import settings

class IPFilterMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response
        #one-time conf and init

    def __call__(self, request):
        #request call:
        allowed_ip_addresses = settings.IPFILTER_MIDDLEWARE['ALLOWED_IP_ADDRESSES']
        client_ip_address = request.META.get('REMOTE_ADDR')
        print(f'** client IP address:{client_ip_address}')

        if not client_ip_address in allowed_ip_addresses:
            raise PermissionDenied

        #response call:
        response = self.get_response(request)

        # Code to be executed for each request/response after the view is called
        response['X-IP-FILTER'] = 'IP FILTER BY KEA'

        return response


