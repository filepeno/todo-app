from django.shortcuts import render, reverse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as dj_login, logout as dj_logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect

def login(request):
    context = {}

    if request.method == "POST":
        user = authenticate(request, username=request.POST['user_name'], password=request.POST['password'])
        if user:
            dj_login(request, user)
            return HttpResponseRedirect(reverse('todo_app:index'))
        else:
            context = {
                    'error': 'Bad username of password'
                    }
    return render(request, 'login_app/login.html', context)

def logout(request):
    dj_logout(request)
    return HttpResponseRedirect(reverse('login_app:login'))

def sign_up(request):
    context = {}
    if request.method == 'POST':
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        user_name = request.POST['user_name']
        email = request.POST['email']
        if password != confirm_password:
            context = {'error': 'Passwords do not match'}
        else:
            if User.objects.create_user(user_name, email, password):
                return HttpResponseRedirect(reverse('login_app:login'))
            else:
                context = {
                        'error':'Could not create the user account - please try again'
                        }
    return render(request, 'login_app/sign_up.html', context)

@login_required
def delete_account(request):
    if request.method == "POST":
        if request.POST['confirm_deletion'] == "DELETE":
            user = authenticate(request, username=request.user.username, password=request.POST['password'])
            if user:
                print(f"Deleting user {user}")
                user.delete()
                return HttpResponseRedirect(reverse('login_app:login'))
            else:
                print("Could not delete the account")
    return render(request, 'login_app/delete_account.html')
